% vim:filetype=tex:tabstop=2
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{pkuthss}
[2010/07/23 v1.2beta Peking University dissertation document class]

% 将文档类参数全部传递给 ctexbook 文档类。
% \DeclareOption*{\PassOptionsToClass{\CurrentOption}{ctexbook}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{ctexbook}}
\ProcessOptions\relax
% 此文档类以 ctexbook 为基础，设定页面尺寸为 A4，默认字号为小四号。
% CJKfntef 宏包提供了 \maketitle 中用到的 \CJKunderline 命令。
% fancyhdr 宏包提供了方便使用的命令用于设置页眉、页脚。
\LoadClass[fntef,a4paper,fancyhdr,cs4size]{ctexbook}[2010/01/22]
% 此宏包提供图形支持。
\RequirePackage{graphicx}[1999/02/16]
% 此宏包提供了方便使用的命令用于设置页面布局。
\RequirePackage{geometry}
% 此宏包把所有的英文改成Times New Roman
% \RequirePackage{times}
% 在页面文字较少时也让脚注显示在底部
\RequirePackage[bottom]{footmisc}
\raggedbottom

% 设定页面布局。
% \geometry{height=240mm,width=150mm,includeheadfoot,headheight=1.2em}
\geometry{top=3.5cm,bottom=3.0cm,left=2.7cm,right=2.7cm}
% 设定页眉、页脚样式。
\fancypagestyle{plain}{\fancyhf{}\renewcommand{\headrulewidth}{0pt}}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[CE]{\zihao{5}\songti\@title}
\fancyhead[CO]{\zihao{5}\songti\leftmark}
\fancyfoot[C]{\zihao{5}\songti 第\thepage{}页~~（共~\pageref{LastPage}~页）}
\fancypagestyle{plain}{
  \pagestyle{fancy}
}

% 这些命令用于设定各项文档信息的具体内容。
\newcommand\etitle[1]{\def\@etitle{#1}}
\newcommand\titletype[1]{\def\@titletype{#1}}
\newcommand\eauthor[1]{\def\@eauthor{#1}}
\newcommand\studentid[1]{\def\@studentid{#1}}
\newcommand\school[1]{\def\@school{#1}}
\newcommand\major[1]{\def\@major{#1}}
\newcommand\emajor[1]{\def\@emajor{#1}}
\newcommand\direction[1]{\def\@direction{#1}}
\newcommand\mentor[1]{\def\@mentor{#1}}
\newcommand\ementor[1]{\def\@ementor{#1}}
\newcommand\timespan[1]{\def\@timespan{#1}}
\newcommand\keywords[1]{\def\@keywords{#1}}
\newcommand\ekeywords[1]{\def\@ekeywords{#1}}

% 此命令用下划线填充指定的空间。
\newcommand{\fillinblank}[2]{\CJKunderline{\makebox[#1]{#2}}}
% 此命令将对象放到页面底部。
\newcommand{\putonbottom}[1]{\begin{figure}[b!]%
    \begin{minipage}{\textwidth}{#1}\end{minipage}\end{figure}}

\renewcommand\tableofcontents{%
  \chapter*{\contentsname}
  \@starttoc{toc}}

% 将列表的key斜体左对齐，value均缩进一段固定长度
\newenvironment{Denotation}[1]{\begin{list}{}{\renewcommand{\makelabel}[1]{%
        \textsl{##1}\hfil}\settowidth\labelwidth{\makelabel{#1}}\itemsep=-5pt%
      \setlength{\leftmargin}{%
        \labelwidth+\labelsep}}}{\end{list}}

% 设定标题页格式。
\renewcommand\maketitle{%
  \cleardoublepage
  \begin{titlepage}
    % 居中。
    \begin{center}
      % 第一部分：校徽、“长江大学”字样和论文类别名。
      {
        % \includegraphics[height=0.1\textheight]{img/yangtzelogo}\hspace{1em}
        \includegraphics[height=0.1\textheight]{img/yangtzeword}\\%[2.25em]
        % {\zihao{1}\bfseries\thesisname}
        {\zihao{5}\heiti\label@euniversity}
      }
      \vfill
      % 第二部分：论文标题。
      {
        \zihao{0}\kaishu{毕业论文（设计）}
        % \makebox[0pt][t]{%
        % \begin{minipage}[t]{12em}
        % \centering\textbf{\@title}
        % \end{minipage}%
        % }
        % \parbox[t]{12em}{\fillinblank{12em}{}\\ \fillinblank{12em}{}}\par
      }
      \vfill
      % 第三部分：作者信息。
      {
        \zihao{-2}
        \begin{tabular}{l@{\extracolsep{0.3em}}c}
          {\kaishu\label@title}     & \fillinblank{17em}{\kaishu\@title}
          \\[5pt]
          {\kaishu\label@titletype} & \fillinblank{17em}{\kaishu\@titletype}
          \\[5pt]
          {\kaishu\label@author}    & \fillinblank{17em}{\kaishu\@author}
          \\[5pt]
          % {\kaishu\label@studentid} & \fillinblank{17em}{\kaishu\@studentid}
          % \\[5pt]
          {\kaishu\label@school}    & \fillinblank{17em}{\kaishu\@school}
          \\[5pt]
          {\kaishu\label@major}     & \fillinblank{17em}{\kaishu\@major}
          \\[5pt]
          % {\kaishu\label@direction} & \fillinblank{17em}{\kaishu\@direction}
          % \\[5pt]
          {\kaishu\label@mentor}& \fillinblank{17em}{\kaishu\@mentor}
          \\[5pt]
          {\kaishu\label@timespan}  & \fillinblank{17em}{\kaishu\@timespan}
        \end{tabular}
      }
      \vfill
      % 第四部分：日期。
      % {\kaishu\zihao{2}\@date}
    \end{center}
  \end{titlepage}
}

% 此命令用于开始不标号的一章。
% 通过 \markboth 命令设置相应的页眉内容（尽管可能不显示）。
\newcommand{\specialchap}[1]{%
  \chapter*{#1}\markboth{#1}{}
  \addcontentsline{toc}{chapter}{#1}\phantomsection%
}

% 此环境用于排版中文摘要。
\newenvironment{cabstract}{%
  \begin{center}
    \zihao{-2}\textbf{\@title}
  \end{center}
  \markboth{\@title}{}
  \vspace{0em}\par
  \begin{center}
    \zihao{-4}\fangsong%
    学\hspace{2em}生：{\@author}\quad{\@major}\\
    指导老师：{\@mentor}\quad{\@major}
  \end{center}
  \vspace{1em}\par
  %\section*{\zihao{4}\cabstractname}
  %\vskip 0.5em%
  \cabstractname
}{% 在下一段排版关键词，下面这个空行***不能***删除

  \label@keywords\@keywords
}

% 此环境用于排版英文摘要。代码的解释详见 cabstract 环境的定义部分。
\newenvironment{eabstract}{%
  \begin{center}
    \zihao{-2}\@etitle
  \end{center}
  \markboth{\@etitle}{}
  \vspace{0em}\par
  \begin{center}
    \zihao{-4}
    Student: {\@eauthor}\quad{\@emajor}\\
    Teacher: {\@ementor}\quad{\@emajor}
  \end{center}
  \vspace{1em}\par
  %\section*{\zihao{4}\eabstractname}
  %\vskip 0.5em%
  \eabstractname
}{

[Keywords]: \@ekeywords}

% % 此环境主要用于设置专业词汇
% \newcommand{\denotation@name}{主要符号对照表}
% \newenvironment{denotation}[1][2.5cm]{
% \specialchap{\denotation@name} % no tocline
% \noindent\begin{list}{}%
%   {\vskip-30bp\zihao{4}[1.6]
%   \renewcommand\makelabel[1]{##1\hfil}
%   \setlength{\labelwidth}{#1} % 标签盒子宽度
%   \setlength{\labelsep}{0.5cm} % 标签与列表文本距离
%   \setlength{\itemindent}{0cm} % 标签缩进量
%   \setlength{\leftmargin}{\labelwidth+\labelsep} % 左边界
%   \setlength{\rightmargin}{0cm}
%   \setlength{\parsep}{0cm} % 段落间距
%   \setlength{\itemsep}{0cm} % 标签间距
%   \setlength{\listparindent}{0cm} % 段落缩进量
%   \setlength{\topsep}{0pt} % 标签与上文的间距
% }}{\end{list}}

% 设置章节标题格式
\CTEXsetup[name={,}]{chapter}
\CTEXsetup[number={\arabic{chapter}}]{chapter}
\CTEXsetup[format={\noindent}]{chapter}
\CTEXsetup[nameformat={\bfseries\heiti \zihao{-2}}]{chapter}
\CTEXsetup[titleformat={\bfseries\heiti \zihao{-2}}]{chapter}
% \CTEXsetup[beforeskip={6pt minus -22pt}]{chapter}
\CTEXsetup[beforeskip={-22pt}]{chapter}
\CTEXsetup[afterskip={6pt}]{chapter}

\CTEXsetup[format={\noindent}]{section}
\CTEXsetup[nameformat={\bfseries\heiti \zihao{3}}]{section}
\CTEXsetup[titleformat={\bfseries\heiti \zihao{3}}]{section}
\CTEXsetup[beforeskip={5pt}]{section}
\CTEXsetup[afterskip={5pt}]{section}

\CTEXsetup[format={\noindent}]{subsection}
\CTEXsetup[nameformat={\bfseries\zihao{-4} \heiti}]{subsection}
\CTEXsetup[titleformat={\bfseries\zihao{-4} \heiti}]{subsection}
\CTEXsetup[beforeskip={3pt}]{subsection}
\CTEXsetup[afterskip={3pt}]{subsection}

\CTEXsetup[format={\noindent}]{subsubsection}
\CTEXsetup[nameformat={\bfseries\zihao{-4} \songti}]{subsubsection}
\CTEXsetup[titleformat={\bfseries\zihao{-4} \songti}]{subsubsection}
\CTEXsetup[beforeskip={3pt}]{subsubsection}
\CTEXsetup[afterskip={3pt}]{subsubsection}

% 设定各处标题的内容。
\AtEndOfClass{\input{pkuthss.def}}
\endinput



